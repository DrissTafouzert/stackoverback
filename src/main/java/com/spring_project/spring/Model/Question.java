/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring_project.spring.Model;

import java.util.List;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration; 
import org.springframework.data.mongodb.core.mapping.Document;




/**
 *
 * @author Administrator
 */
@EnableAutoConfiguration
@Getter
@Setter
@ToString

@Document(collection = "Question")

public class Question 
{
    private String _id;
    private String _id_user;
    private String titre;
    private String Description;
    private String date;
    private List images;
    
}
