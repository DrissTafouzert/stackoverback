/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring_project.spring.Model;
 
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration; 
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;




/**
 *
 * @author Administrator
 */
@EnableAutoConfiguration
@Getter
@Setter
@ToString

@Document(collection = "User")
public class User 
{
   private String _id;
   private String name;
   private String email;
   private boolean admin;
}
