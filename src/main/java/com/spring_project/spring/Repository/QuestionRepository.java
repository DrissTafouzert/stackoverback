/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring_project.spring.Repository;
 
import com.spring_project.spring.Model.Question;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.mongodb.repository.MongoRepository; 
import org.springframework.stereotype.Repository; 

/**
 *
 * @author Administrator
 */
@EnableAutoConfiguration
@Repository
public interface QuestionRepository extends MongoRepository<Question, Integer>
{
    
}
