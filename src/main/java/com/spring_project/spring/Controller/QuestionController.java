/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring_project.spring.Controller;



import com.spring_project.spring.Model.User;
import com.mongodb.client.MongoClients;
import com.spring_project.spring.Model.Question;
import com.spring_project.spring.Repository.QuestionRepository;
import com.spring_project.spring.Repository.UserRepository;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
/**
 *
 * @author Administrator
 */
@RestController
@EnableAutoConfiguration
public class QuestionController 
{
    @Autowired
    private QuestionRepository repository;
    private List<String> images=new ArrayList<String>();
    
    @CrossOrigin() 
    @PostMapping("/saveQuestion2")
    public void saveQuestion(@RequestBody Question question)
    { 
        question.setImages(images);
        this.repository.save(question);  
       
    }
    
    @CrossOrigin()
    @PostMapping("/loadimage") 
    public void  loadImage(@RequestParam Map<String,String> allParams, 
            @RequestParam("attachment") MultipartFile file)
    { 
        //String name=file.getSubmittedFileName();
       
         try{
            InputStream input=file.getInputStream();
            String name=file.getOriginalFilename();
            File f=new File("/Users/Administrator/Downloads/spring/src/main/resources/"+name);
            this.images.add(name);
            if(!f.exists()){
                f.createNewFile();
            }
            FileOutputStream output=new FileOutputStream(f);
            byte[] buffer=new byte[1024];
            int length;
            while((length=input.read(buffer))>0){
                output.write(buffer, 0, length);
            }
            
            input.close();
            output.close();
        }catch(Exception e){
            e.printStackTrace(System.out);
        }
       System.out.println(" Image load now nihhahahhahahah  "+allParams.entrySet()+"  "+file.getOriginalFilename());
    }
    
}
