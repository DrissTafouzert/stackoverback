package com.spring_project.spring.Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.spring_project.spring.Model.User;
import com.mongodb.client.MongoClients;
import com.spring_project.spring.Model.Question;
import com.spring_project.spring.Repository.UserRepository;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 *
 * @author Administrator
 */

@RestController
@EnableAutoConfiguration
public class UserController 
{
   @Autowired
    private UserRepository repository;
    private List<String> images;
    public static User currentUser;
    private QuestionController questionController =new QuestionController();
    
    @CrossOrigin()
    @PostMapping("/addUser") 
    public String saveUser(@RequestBody User user)
    {
        repository.save(user);
        return user.getName()+"/";
    }
    
    
    @CrossOrigin() 
    @PostMapping("/saveQuestion")
    public void saveQuestion(@RequestParam Map<String,String> allParams,@RequestBody Question question)
    {
        //System.out.println(" save question  "+allParams.entrySet()+"  "+question.getTitre()+" " +currentUser.getName());
      
        
       
       
    }
    
    
    @CrossOrigin() 
    @GetMapping("/users")
    public List<User> getAllUser()
    { 
        List<com.spring_project.spring.Model.User> list= repository.findAll(); 
        return list;
    }
    
    @CrossOrigin() 
    @GetMapping("/login")
    public User login(@RequestParam String email)
    {
        //User user=new User();
        //User user= repository..find({email:emaill});
        Query q=new Query();
        q.addCriteria(Criteria.where("email").is(email));
        MongoOperations mongoOps = new MongoTemplate(MongoClients.create(), "stackover");
        List <User> users;
        User user=mongoOps.findOne(new Query(where("email").is(email)), User.class);
       //users = MongoTemplate.find(q, User.class);
        System.out.println(email+"  size of list of users");
        this.currentUser=user;
        return user;
    }
    
}
